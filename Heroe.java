/**
 * Esta clase implementatoda la funcionalidad del héroe.
 *
 * @author Joan Mendaro
 * @version 1.0
**/

public class Heroe extends Personaje {

	private int espInventario = 0;
	private Objeto[] inventario = new Objeto[10];
	Objeto encontrado;

	/**
	* Constructor con 5 parámetros
	*
	* @param nombre del personaje, parametro extendido de la clase Personaje
	* @param dano del personaje, parametro extendido de la clase Personaje
	* @param defensa del personaje, parametro extendido de la clase Personaje
	* @param vida del personaje, parametro extendido de la clase Personaje
	* @param dinero del jugador, parametro extendido de la clase Personaje
	*/

	public Heroe(String nombre, int dano, int defensa, int vida, int dinero) {
		super(nombre, dano, defensa, vida, dinero);
	}

/**
 * Método que sirve para calcular el daño que hará el héroe a partir de la vida del monstruo
 */
	public void atacar (Monstruo monst ) {
		//Calculamos el ataque que se le va ha hacer al monstruo
		double attack = ((1-(((double)monst.getDefensa()*2)/100))* (double)this.getDano());
		//Y lo mostramos por pantalla
		System.out.println("Ataque de heroe " + (int)attack);

 		//Le restamos la vida al Monstruo
		monst.setVida(monst.getVida() - (int)attack);
		//y mostramos cuanta vida le queda al monstruo
		System.out.println("El monstruo tiene: " + monst.getVida() + " puntos de Vida");
	}

/**
 * Método que nos permitirá buscar objeto al final de turno y cambiará las estadísticas a partir del objeto
 */
	public int buscarObj() {
		//Buscamos un numero del 0 al 4
		int randObj = (int)(Math.random() * 4);

		//dependiendo de que numero salga sera un objeto o en caso de que salga 4 no dara nada
		switch (randObj) {
		case 1:
			System.out.println("Has encontrado arma");
			 encontrado = new Arma("espada", 300, 5);

			break;
		case 2:
			System.out.println("Has encontrado una pocion");
			 encontrado = new Pociones("Coccion de ponfusion", 200, 6);

			break;
		case 3:
			System.out.println("Has encontrado una armadura");
			 encontrado = new Armadura("Pechera de diamante", 500, 9);

			break;
		case 4:
			System.out.println("No has encontrado nada");

			break;
		default:
			System.out.println("Has encontrado una pocin");
			 encontrado = new Hechizos("Rayo", 250, 5, "Rayo");
			break;

		}

		return randObj;
	}

/**
 * Método que comprueba si al recojer el objeto sigue quedando espacio en el inventario
 */
	public void reducirInventario(int tamano) {
		//miramos si el inventario esta lleno
		if(espInventario>=10) {
			System.out.println("No hay espacion en el inventario");
			//si no lo esta podremos añadir el objeto
		}else {
			inventario[espInventario] = encontrado;
			System.out.println("Se ha aadido a tu inventario");

			espInventario = espInventario + tamano;

		}

	}
	//metodos set y get y otros que se añadiran proximamente

	public void lanzarHechizo(Monstruo mnst) {

	}

/**
* Getters y setters del espacio del inventario
*/
	public int getInventario() {
		return espInventario;
	}
	public void setInventario(int inventario) {
		this.espInventario = inventario;
	}
	public void defensa() {

	}
	public void lanzaHechizos() {

	}

	public int getEspInventario() {
		return espInventario;
	}

	public void setEspInventario(int espInventario) {
		this.espInventario = espInventario;
	}

	public Objeto[] getinventario() {
		return inventario;
	}

	public void setinventario(Objeto[] inventario) {
		this.inventario = inventario;
	}

	public Objeto getEncontrado() {
		return encontrado;
	}

	public void setEncontrado(Objeto encontrado) {
		this.encontrado = encontrado;
	}

}
