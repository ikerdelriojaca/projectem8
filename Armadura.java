/**
 * Clase Defensa
 *
 * Contiene informacion de cada Defensa
 *
 * @author Alejandro Lozano
 * @version 1.15
 */
public class Armadura extends Objeto {
	// Atributos
	/* da�o que evita cuando le atacan */
	private int armadura;

	// Constructores
	/**
	 * Constructor con 3 parametros
	 * 
	 * @param nombre  de la Defensa , parametro extendido de la clase Objeto
	 * @param valor   que tiene la defensa en el mercado, parametro extendido de la
	 *                clase Objeto
	 * @param armadura que tiene la armadura para evitar el da�o del ataque
	 */
	public Armadura(String nombre, int valor, int armadura) {
		super(nombre, valor);
		this.armadura = armadura;
	}

}
