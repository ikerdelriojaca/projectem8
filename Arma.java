/**
 * Clase Arma
 *
 * Contiene informacion de cada Arma
 *
 * @author Alejandro Lozano
 * @version 1.0
 */
public class Arma extends Objeto {
	// Atributos
	/* da�o cuando se ataca con la arma */
	private int dano;

	// Constructores
	/**
	 * Constructor con 3 parametros
	 * 
	 * @param nombre de la arma, parametro extendido de la clase Objeto
	 * @param valor  que tiene la arma en el mercado, parametro extendido de la
	 *               clase Objeto
	 * @param da�o   que hace la arma cuando se ataca con ella
	 */
	public Arma(String nombre, int valor, int dano) {
		super(nombre, valor);
		this.dano = dano;
	}

}
