/**
 * Clase Hechizos
 *
 * Contiene informacion de cada Objeto
 *
 * @author Alejandro Lozano
 * @version 1.15
 */

public class Objeto {
	// Atributos
	/* nombre del objeto */
	protected String nombre;
	/* valor que tiene el objeto en el mercado */
	protected int Valor;

	// Constructores
	/**
	 * Constructor con 2 parametros
	 * 
	 * @param nombre del objeto
	 * @param valor  que tiene el objeto en el mercado
	 * 
	 */
	public Objeto(String nombre, int valor) {
		this.nombre = nombre;
		Valor = valor;
	}

}
