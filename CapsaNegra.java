import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CapsaNegra {

	@Test
	void heroeGanaAUnMonstruo() {
		Heroe heroe = new Heroe("Naoj", 4, 10, 6, 80);
		Monstruo monst = new Monstruo("Nairda", 2, 10, 2, 120);
		heroe.atacar(monst);
		assert(monst.getVida() <= 0);
	}
	
	@Test
	void monstruoGanaAUnHeroe() {
		Heroe heroe = new Heroe("Naoj", 4, 10, 2, 80);
		Monstruo monst = new Monstruo("Nairda", 2, 10, 10, 120);
		monst.atacar(heroe);
		assert(heroe.getVida() <= 0);
	}
	
	@Test
	void verSiBuscarObjetoRetornaUnValorEntreZeroYQuatro() {
		Heroe heroe = new Heroe("Naoj", 4, 10, 2, 80);
		int obj = heroe.buscarObj();
		assert(obj == 0 ||obj == 1 ||obj == 2 ||obj == 3 || obj == 4 );
	}
	
	@Test
	void comprobarQueOcupaUnEspacioEneLInventario() {
		Heroe heroe = new Heroe("Naoj", 4, 10, 2, 80);
		heroe.reducirInventario(4);
		
		assert(heroe.getEspInventario() == 4);
	}
}
