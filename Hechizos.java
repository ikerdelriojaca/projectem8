/**
 * Clase Hechizos
 *
 * Contiene informacion de cada hechzo
 *
 * @author Alejandro Lozano
 * @version 1.15
 */
public class Hechizos extends Objeto {
	// Atributos
	/* da�o que hace al atacar */
	private int dano;
	/* define el tipo de hechizo que es (fuego, agua, viento,planta) */
	private String tipo;

	// Constructores
	/**
	 * Constructor con 4 parametros
	 * 
	 * @param nombre del hechizo , parametro extendido de la clase Objeto
	 * @param valor  que tiene el hechizo en el mercado, parametro extendido de la
	 *               clase Objeto
	 * @param dano   que tiene el hechizo cuando se ataca con ella
	 * @param tipo   de hechizo que se utiliza para atacar
	 */
	public Hechizos(String nombre, int valor, int dano, String tipo) {
		super(nombre, valor);
		this.dano = dano;
		this.tipo = tipo;
	}
}