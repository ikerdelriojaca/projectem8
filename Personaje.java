/**
 * Esta clase define todas las variables base que se utilizarán para héroe y mónstruo
 *
 * @author Joan Mendaro
 * @version 1.0
**/

public class Personaje {
	private String nombre;
	private int dano;
	private int defensa; 
	private int vida;
	private int dinero;
	
	/**
	* Constructor con 5 parámetros
	* 
	* @param nombre es el nombre del personaje
	* @param dano es el daño base que hará el personaje
	* @param defensa es la defensa base del personaje
	* @param vida es la vida base del personaje
	* @param dinero es el dinero con el que empieza el jugador
	*/
	public Personaje( String nombre, int dano, int defensa, int vida, int dinero) {
		super();
		this.nombre = nombre;
		this.dano = dano;
		this.defensa = defensa;
		this.vida = vida;
		this.dinero = dinero;
	}

	/**
	*Getters y setters de todas las variables de la clase
	*/
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDano() {
		return dano;
	}
	public void setDano(int dano) {
		this.dano = dano;
	}
	public int getDefensa() {
		return defensa;
	}
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
	public int getDinero() {
		return dinero;
	}
	public void setDinero(int dinero) {
		this.dinero = dinero;
	}
	
	
	
}
