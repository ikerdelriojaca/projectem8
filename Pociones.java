/**
 * Clase Pociones
 *
 * Contiene informacion de cada poci�n
 *
 * @author Alejandro Lozano
 * @version 1.15
 */
public class Pociones extends Objeto {
	// Atributos

	/**
	 * vida que da cada pocion.
	 */
	int HP;
	/**
     * Constructor con  parametros
     * @param nombre de la pocion
	 * @param valor  que tiene la poci�n en el mercado
     * @param HP es la vida que te sube cada vez que te tomas una poci�n de ese tio.
     * 
     */
	public Pociones(String nombre, int valor, int hP) {
		super(nombre, valor);
		this.HP = hP;
	}

}
