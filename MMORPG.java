import java.util.Scanner;

/**
 * Clase que se utilizará para testear las clases
 *
 * @author Joan Mendaro
 * @version 1.0
*/
public class MMORPG {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner (System.in);
		
		//Se llaman las clases heroe y monstruo para empezar el juego
		Heroe heroe = new Heroe("Naoj", 4, 10, 6, 80);
		Monstruo monst = new Monstruo("Nairda", 2, 10, 8, 120);
		
		boolean combate = true;
		int ganador = 0;
		
		ganar(combate, heroe, monst, jugar(combate, heroe, monst, ganador));


	}
	
	/**
	*Classe que iniciará la partida pasando cuatro variables
	* @param combate que mira si la partida está empezada
	* @param heroe que dice el héroe con el que se está jugando
	* @param monst que pa el monstruo con el que se está jugando
	* @param ganador que dice el ganador del último juego
	*/
	
	public static int jugar(boolean combate, Heroe heroe, Monstruo monst, int ganador) {
		
		//mientras el heroe y el monstruo tengan vida se seguirá la partida
		while(combate) {
			
			//empieza atacando el héroe
			heroe.atacar(monst);
	
			//Si el monstruo se queda sin vida se devuelve ganador = 1 y se termina la partida
			if(monst.getVida() <= 0) {
				combate = false;
				System.out.println("Pierde montruo");
				ganador = 1;
			}

		//Si la partida no se ha terminado ataca el monstruo
			if(combate) {
				monst.atacar(heroe);
			}
	
		//Si se mata al héroe se termina la partida
			if(heroe.getVida() <= 0){
				combate = false;
				System.out.println("Pierde Heroe");
			}
		}
		
		//Se cambian las vidas de los personajes para que se pueda seguir jugando ya que sino tienen muy 
		//poca vida para hacer una partida entera
		monst.setVida(8);
		heroe.setVida(heroe.getVida()+10);
		
		//se devuelve el ganador
		return ganador;
	}
	
	/**
	* clase que dice si quieres volver a jugar o no al matar al monstruo
	* @param combate que mira si la partida está empezada
	* @param heroe que dice el héroe con el que se está jugando
	* @param monst que pa el monstruo con el que se está jugando
	* @param ganador que dice el ganador del último juego
	*/
	public static void ganar(boolean combate, Heroe heroe, Monstruo monst, int ganador) {
		Scanner teclado = new Scanner(System.in);
		
		//Si ha ganado el héroe se pregunta si quieres buscar objetos
		if(ganador == 1) {
			System.out.println("Has matado al monstruo quieres ver si hay algun objeto en la sala?");
			
			//Se busca un objeto
			String buscar = teclado.next();
			int objEncontrado = 0;
			
			//Se pregunta si se quiere ver si hay objeto
			if(buscar.equalsIgnoreCase("Si")){
				objEncontrado = heroe.buscarObj();
				
				//Si no es 4(el -1 típico, que no hay objeto)
				if(objEncontrado != 4) {


					System.out.println("Quieres el Objeto?");

					if(teclado.next().equalsIgnoreCase("Si")) {
						
						//Si lo coje se quita el espacio que ocupa el objeto en el inventario
						heroe.reducirInventario(objEncontrado);
					}else {
						//Sino lo suelta
						System.out.println("Has soltado el Objeto");
					}

				}
				//Te dice cuantos espacios libres quedan en el inventario
				System.out.println("Tienes " + (10-heroe.getInventario()) + " huecos en el inventario");
			}

			//Se pregunta si quiere seguir luchando, si es asi se llama ganar otra vez
			System.out.println("¿Quieres seguir luchando?");
			if(teclado.next().equalsIgnoreCase("si")) {
				
				ganar(combate, heroe, monst, jugar(combate, heroe, monst, ganador));
			}
		}else {
			System.out.println("Has muerto");
		}
		

	}
}
