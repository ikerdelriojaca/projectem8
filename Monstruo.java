/**
 * Esta clase define el funcionamiento básico del monstruo
 *
 * @author Joan Mendaro
 * @version 1.0
**/
public class Monstruo extends Personaje {

	/**
	* Constructor con 5 parámetros
	*
	* @param nombre del personaje, parametro extendido de la clase Personaje
	* @param dano del personaje, parametro extendido de la clase Personaje
	* @param defensa del personaje, parametro extendido de la clase Personaje
	* @param vida del personaje, parametro extendido de la clase Personaje
	* @param dinero del jugador, parametro extendido de la clase Personaje
	*/

	public Monstruo(String nombre, int dano, int defensa, int vida, int dinero) {
		super(nombre, dano, defensa, vida, dinero);
	}

	/**
	*Método que calcula el ataque con el @param her, éste método funciona creando una relación entre la vida
	*general, para no sacar demasiado ni demasiado poco
	*/
	public void atacar (Heroe her ) {
		//Para gestionar el ataque que le hace el Monstruo al Heroe hemos creado la siguiente formula:
		//la defensa de el heroe la multiplicamos por 2 y ese resultado lo dividimos entre 100, dara un numero menor que uno.
		//el numero obtenido es restado por 1 por lo que saldra un numero positivo menor que uno
		//este numero es multiplicado por el daño que hace el monstruo.
		//con esta formula hemos tenido en cuenta la defensa que tiene el heroe y como actua el ataque del monstruo sobre ella.
		int attack = ((1-((her.getDefensa()*2)/100))* this.getDano());
		//acctualizamos la vida del heroe
		her.setVida(her.getVida() - attack);
		//informacion para el usuario sobre el transcurso del juego.
		System.out.println("ataque de Monstruo " + attack);
		System.out.println("El heroe tiene: " + her.getVida() + " puntos de Vida");
	}
}
